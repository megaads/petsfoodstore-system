<?php

namespace App\Http\Controllers\System;


use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index() {
        $sites = config('member.sites');
        return view('system.category.mapping.index', ['sites' => $sites]);
    }
}
