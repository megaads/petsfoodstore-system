<?php
/**
 * Created by PhpStorm.
 * User: DiemND
 * Date: 12/26/2018
 * Time: 11:39
 */

namespace App\Http\Controllers\System;


use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index () {
        return view('system.home.index');
    }
}