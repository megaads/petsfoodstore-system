<?php
/**
 * Created by PhpStorm.
 * User: DiemND
 * Date: 12/27/2018
 * Time: 14:28
 */

namespace App\Http\Controllers\System;


use App\Http\Controllers\Controller;

class ProductMatchingController extends Controller
{
    public function index() {
        return view('system.product-matching.index');
    }
}