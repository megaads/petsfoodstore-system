<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/system/product-matching');
});
Auth::routes();
Route::group(['prefix' => 'system', 'namespace' => 'System', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('system::home::index');
    Route::get('/product-matching', 'ProductMatchingController@index')->name('system::product-matching::index');
    Route::get('/category-mapping', 'CategoryController@index')->name('system::category-mapping::index');
});
