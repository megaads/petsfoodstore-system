<?php

return [
    "sites" => [
        ["domain" => "chewy.com", "name" => "Chewy.com"],
        ["domain" => "petco.com", "name" => "Petco.com"],
        ["domain" => "petsmart.com", "name" => "Petsmart.com"]
    ]
];
