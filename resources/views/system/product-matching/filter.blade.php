<div class="box" style="margin-bottom: 0px">
    <div class="box-body" style="padding-bottom: 0px;">
        <form class="form-horizontal">
            {{--<div class="col-md-3">--}}
                {{--<div class="form-group">--}}
                    {{--<label class="control-label col-md-4">Name </label>--}}
                    {{--<div class="col-md-8">--}}
                        {{--<input class="form-control" type="text" name="" value="" ng-model="filter.name"--}}
                               {{--ng-keyup="$event.keyCode == 13 && getDeals()">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label col-md-4">From </label>
                    <div class="col-md-8">
                        <input type="text"
                               ng-model="filter.create_from"
                               class="form-control pull-right datepicker date-from">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label col-md-4">To </label>
                    <div class="col-md-8">
                        <input type="text"
                               ng-model="filter.create_to"
                               class="form-control pull-right datepicker date-to">
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <button type="button" id="searchDealButton" class="btn btn-success search" ng-click="find()"
                        data-loading-text="<span class='fa fa-spinner fa-spin'></span> Loading..."><i
                            class="fa fa-search"></i> Search
                </button>
                <button type="button" class="btn btn-warning btn-reset" ng-click="reset()"><i class="fa fa-times"></i>
                    Reset
                </button>
            </div>

        </form>


    </div>
    <!-- /.box-body -->
</div>
<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
</script>
