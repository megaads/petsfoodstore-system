<style>
    .link-image {
        height: 110px;
        padding: 0px;
    }
    .link-image img {
        max-height: 110px; max-width: 100%
    }
    .product-info p {
        line-height: 13px;
    }
</style>
<div class="box box-solid">
    <div class="box-body">

        <table class="table table-bordered" style="font-size: 12px;">
            <tbody>
                <tr>
                    <th style="width: 2%">#</th>
                    <th class="" style="text-align: center; width: @{{floor(80, listMatchedProduct.length + 1)}}%;">Product</th>
                    <th class="" ng-repeat="i in listMatchedProduct" style="text-align: center; width: @{{floor(80, listMatchedProduct.length + 1)}}%;">
                        Matched Product @{{ i + 1 }}
                    </th>
                    <th class="col-md-1"></th>
                </tr>
                <tr ng-repeat="item in items">
                    <td style="line-height: 110px">@{{$index + 1}}</td>
                    <td>
                        <a href="@{{ item.meta_data.product.url }}" target="_blank" class="col-md-3 link-image">
                            <img src="@{{ item.meta_data.product.image_url }}">
                        </a>
                        <div class="col-md-9 product-info">
                            <p><b>Site</b>: @{{ item.meta_data.product.site }}</p>
                            <p><b>Name</b>: @{{ item.meta_data.product.name }}</p>
                            <p><b>Code</b>: @{{ item.meta_data.product.code }}</p>
                            <p><b>Price</b>: $@{{ item.meta_data.product.price }}</p>
                            <p><b>Brand</b>: @{{ item.meta_data.product.brand_name }}</p>
                        </div>
                    </td>
                    <td ng-repeat="i in listMatchedProduct">
                        <a ng-if="item.meta_data.matched_products[i].id" href="@{{ item.meta_data.matched_products[i].url }}" target="_blank" class="col-md-3 link-image">
                            <img src="@{{ item.meta_data.matched_products[i].image_url }}">
                        </a>
                        <div class="col-md-9 product-info" ng-if="item.meta_data.matched_products[i].id">
                            <p><b>Site</b>: @{{ item.meta_data.matched_products[i].site }}
                                <i class="fa fa-times fa-2x" ng-click="unMatchProduct(item, item.meta_data.matched_products[i].id)" aria-hidden="true" style="cursor: pointer; float: right; color: firebrick"></i>
                            </p>
                            <p><b>Name</b>: @{{ item.meta_data.matched_products[i].name }}</p>
                            <p><b>Code</b>: @{{ item.meta_data.matched_products[i].code }}</p>
                            <p><b>Price</b>: $@{{ item.meta_data.matched_products[i].price }}</p>
                            <p><b>Brand</b>: @{{ item.meta_data.matched_products[i].brand_name }}</p>
                        </div>
                    </td>
                    <td style="text-align: center">
                        <i class="fa fa-times fa-2x" ng-click="unMatchProduct(item, null)" aria-hidden="true" style="line-height: 110px; cursor: pointer; color: firebrick"></i>
                    </td>
                </tr>
              </tbody>
          </table>
          <div class="center" style="margin-top: 20px">
              @include('system.common.pagination', [
                'accessPageId' => 'filter.pageId',
                'accessPagesCount' => 'recordsCount',
                'accessFind' => 'findMatched()'
                ])
          </div>
    </div>
</div>
