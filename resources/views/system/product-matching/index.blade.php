@extends('system.layouts.main')
@section('script')
<script src="/sys/js/script/bootstrap-datepicker.js?v={{ env('APP_VERSION') }}" charset="utf-8"></script>
<script src="/sys/js/controller/product-matching-controller.js?v={{ env('APP_VERSION') }}" charset="utf-8"></script>
@endsection
@section('content')
    <section class="content-header" style="padding-bottom: 15px">
        <h1>
            Manage Product Matching
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manage Product Matching</li>
        </ol>
    </section>
    <div class="nav-tabs-custom" ng-controller="ProductMatchingController">
        @include('system.product-matching.filter')
        <ul class="nav nav-tabs">
            <li class="active" ng-click="findMatched()"><a href="#create_tab_1" data-toggle="tab">Matched</a></li>
            <li ng-click="clickTabSuggestion()"><a href="#create_tab_2"  data-toggle="tab">Suggestion</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="create_tab_1">
                <div class="row">
                    @include('system.product-matching.list')
                </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="create_tab_2">
                <div class="row">
                    @include('system.product-matching.list-suggestion')
                </div>
            </div>
        </div>
    </div>

@endsection
