<style>
    .tooltip-x {
        position: relative;
        display: inline-block;
    }

    .tooltip-x .tooltiptext {
        visibility: hidden;
        width: 200px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 1;
        bottom: 125%;
        left: 50%;
        margin-left: -60px;
        opacity: 0;
        transition: opacity 0.3s;
    }

    .tooltip-x .tooltiptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    .tooltip-x:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
<div class="box box-solid">
    <div class="box-body">

        <table class="table table-bordered" style="font-size: 12px;">
            <tbody>
                <tr>
                    <th style="width: 2%">#</th>
                    <th class="" style="text-align: center; width: @{{floor(98, listSuggestionProduct.length + 1)}}%;">Product</th>
                    <th class="" ng-repeat="i in listSuggestionProduct" style="text-align: center; width: @{{floor(98, listSuggestionProduct.length + 1)}}%;">
                        Suggestion @{{ i + 1 }}
                    </th>
                </tr>
                <tr ng-repeat="item in itemsSuggestion">
                    <td style="line-height: 110px">@{{$index + 1}}</td>
                    <td>
                        <a href="@{{ item.meta_data.product.url }}" target="_blank" class="col-md-3 link-image">
                            <img src="@{{ item.meta_data.product.image_url }}">
                        </a>
                        <div class="col-md-9 product-info">
                            <p>
                                <b>Site</b>: @{{ item.meta_data.product.site }}
                                <i class="fa fa-window-close-o fa-2x" ng-click="matchSuggestion($index, item, 0)" aria-hidden="true" style="cursor: pointer; float: right; color: crimson"></i>
                            </p>
                            <p><b>Name</b>: @{{ item.meta_data.product.name }}</p>
                            <p><b>Code</b>: @{{ item.meta_data.product.code }}</p>
                            <p><b>Price</b>: $@{{ item.meta_data.product.price }}</p>
                            <p><b>Brand</b>: @{{ item.meta_data.product.brand_name }}</p>
                        </div>
                    </td>
                    <td ng-repeat="i in listSuggestionProduct">
                        <a ng-if="item.meta_data.similar_products[i].id" href="@{{ item.meta_data.similar_products[i].url }}" target="_blank" class="col-md-6 link-image tooltip-x">
                            <img src="@{{ item.meta_data.similar_products[i].image_url }}">
                            <span class="tooltiptext">
                                <p><b>Site</b>: @{{ item.meta_data.similar_products[i].site }}</p>
                                <p><b>Name</b>: @{{ item.meta_data.similar_products[i].name }}</p>
                                <p><b>Code</b>: @{{ item.meta_data.similar_products[i].code }}</p>
                                <p><b>Price</b>: $@{{ item.meta_data.similar_products[i].price }}</p>
                                <p><b>Brand</b>: @{{ item.meta_data.similar_products[i].brand_name }}</p>
                            </span>
                        </a>
                        <div class="col-md-6 product-info" ng-if="item.meta_data.similar_products[i].id">
                            <i class="fa fa-handshake-o fa-2x" ng-click="matchSuggestion(0, item, item.meta_data.similar_products[i].id)" aria-hidden="true" style="cursor: pointer; float: right; color: forestgreen"></i>
                            <!--<p><b>Site</b>: @{{ item.meta_data.similar_products[i].site }}

                            </p>
                            <p><b>Name</b>: @{{ item.meta_data.similar_products[i].name }}</p>
                            <p><b>Code</b>: @{{ item.meta_data.similar_products[i].code }}</p>
                            <p><b>Price</b>: $@{{ item.meta_data.similar_products[i].price }}</p>
                            <p><b>Brand</b>: @{{ item.meta_data.similar_products[i].brand_name }}</p>-->
                        </div>
                    </td>
                </tr>
              </tbody>
          </table>
          <div class="center" style="margin-top: 20px">
              @include('system.common.pagination', [
                'accessPageId' => 'filterSuggestion.pageId',
                'accessPagesCount' => 'recordsCountSuggestion',
                'accessFind' => 'findSuggestion()'
                ])
          </div>
    </div>
</div>
