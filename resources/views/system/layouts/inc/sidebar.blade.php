<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/sys/images/no-image.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Control Panel</li>
            <li class="{{ (Route::currentRouteName()=='system::home::index')?'active':'' }}">
                <a href="{{ route('system::home::index' )}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            <li class="{{ (Route::currentRouteName()=='system::category-mapping::index')?'active':'' }}">
                <a href="{{ route('system::category-mapping::index' )}}"><i class="fa fa-map-signs"></i> <span>Category mapping</span></a>
            </li>
            <li class="{{ (Route::currentRouteName()=='system::product-matching::index')?'active':'' }}">
                <a href="{{ route('system::product-matching::index' )}}"><i class="fa fa-product-hunt"></i> <span>Product matching</span></a>
            </li>


            {{--<li class="treeview active">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-credit-card"></i>--}}
                    {{--<span>Category</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu" style="padding-left: 12px">--}}
                    {{--<li class="active"><a href="#"><i class="fa fa-circle-o"></i> Store</a></li>--}}
                    {{--<li class="active"><a href="#"><i class="fa fa-circle-o"></i> Store</a></li>--}}
                    {{--<li class="active"><a href="#"><i class="fa fa-circle-o"></i> Store</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </section>
</aside>
