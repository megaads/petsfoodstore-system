<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="col-md-1">
                    Mappings
                </div>
                <div class="col-md-2">
                    <select chosen class="form-control col-md-3 inpu-sm select2 select2-hidden-accessible"
                            ng-model="filter.targetSite"
                            ng-options="item.name for item in targetSites track by item.domain"
                            ng-change="findTargetTree()">
                            <option value="">Select a mapping site</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" style="border-radius: 6px;height: 26px;" class="form-control col-md-3 input-sm" placeholder="Search" ng-model="predicate">
                </div>
                <div class="col-md-6">
                    <h4 class="box-title pull-right" style="margin-top: 6px;"></h4>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div treecontrol class="tree-light"
                    tree-model="treedata"
                    options="treeOptions"
                    filter-expression="predicate"
                    filter-comparator="comparator"
                    on-selection="showSelected(node)"
                    selected-node="node1"
                    >
                        @{{node.title}}<span ng-show="node.mapping.length > 0"> - (<b>Maps:</b> <span ng-repeat="item in node.mapping">@{{item.name}} <i class="fa fa-times" style="color: #dd4b39 !important;" title="Remove @{{item.name}}" ng-click="removeMapping(node, item)"></i>, </span>)</span>
                        <div class="pull-right" style="margin-right: 10px;" ng-click="fillTargetTree(node)">
                            <select chosen
                                ng-model="node.categoryMap"
                                ng-options="target.name for target in node.treeTarget"
                                ng-change="mappingCategory(node);"
                                style="width: 250px;">
                                <option value="">Choose a category</option>
                            </select>
                            <i class="fa fa-spinner" ng-show="node.isLoading"></i>
                        </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
    </div>
</div>
