<div id="form-category-modal" class="modal modal-success fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><i class="ti-plus"></i> @{{node.title}}</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Site</th>
                        <th>Category</th>
                        <th></th>
                    </tr>
                    <tr ng-repeat="map in maps">
                        <td>@{{$index + 1}}</td>
                        <td>@{{map.maps.site}}</td>
                        <td>@{{map.maps.name}}</td>
                        <td>
                            <button class="btn btn-danger btn-sm" ng-click="removeMapping(map);"><i class="fa fa-fw fa-remove"></i></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal"><i class="ti-close"></i> Đóng</button>
            </div>
        </div>
    </div>
</div>
