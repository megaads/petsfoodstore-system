@extends('system.layouts.main')
@section('script')
<script>
    var urlApi = "{{env('API_URL')}}";
    var siteMain = "{{env('SITE_MAIN')}}";
    var tokenField = "{{env('TOKEN_FIELD')}}";
    var serviceToken = "{{env('SERVICE_TOKEN')}}";
    var sites = <?= json_encode($sites) ?>;
</script>
<script src="/sys/js/script/bootstrap-datepicker.js?v={{ env('APP_VERSION') }}" charset="utf-8"></script>
<script src="/sys/js/controller/category-mapping-controller.js?v={{ env('APP_VERSION') }}" charset="utf-8"></script>

@endsection
@section('content')
    <section class="content-header" style="padding-bottom: 15px">
        <h1>
            Manage Category
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manage Category</li>
        </ol>
    </section>
    <div class="nav-tabs-custom" ng-controller="CategoryMappingController">
        <div class="tab-content">
            <div class="tab-pane active" id="create_tab_1">
                <div class="row">
                    @include('system.category.mapping.list')
                    @include('system.category.mapping.form')
                </div>
            </div>
        </div>
    </div>

@endsection
