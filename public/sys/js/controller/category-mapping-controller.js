system.controller("CategoryMappingController", CategoryMappingController);
/**
 *
 * @param {type} $scope
 * @param {type} $http
 * @param {type} $rootScope
 * @returns {undefined}
 */
function CategoryMappingController($scope, $http, $rootScope, $timeout, Upload) {
    $scope.controllerName = "CategoryMappingController";
    this.__proto__ = new BaseController($scope, $http, $rootScope);
    $scope.treedata = [];

    $scope.treeOptions = {
        nodeChildren: "children",
        dirSelectable: true,
        injectClasses: {
            ul: "a1",
            li: "a2",
            liSelected: "a7",
            iExpanded: "a3",
            iCollapsed: "a4",
            iLeaf: "a5",
            label: "a6",
            labelSelected: "a8"
        }
    }
    $scope.predicate = null;
    $scope.treeTarget = [];
    $scope.node = {};
    $scope.sites = sites;
    $scope.filter = {
    };
    $scope.siteMain = siteMain;
    $scope.mappingSite = null;
    this.initialize = function() {
        $scope.targetSites = [];
        for (var i = 0; i < $scope.sites.length; i++) {
            if ($scope.sites[i].domain != siteMain) {
                $scope.targetSites.push($scope.sites[i]);
            }
        }
        $scope.filter.targetSite = $scope.getByField($scope.targetSites, "domain", $scope.targetSites[0].domain);
        $scope.findTargetTree();
        $scope.findMainTree();
    }

    $scope.mappingCategory = function(node) {
        if (node.categoryMap && node.categoryMap != '') {
            node.isLoading = true;
            var data = {
                "category_id": node.categoryMap.id,
                "target_id": node.id
            };
            data[tokenField] = serviceToken;
            var url = urlApi + "/api/pets_category_mapping";
            $http.post(url, data).success(function (data) {
                if (data.status == 'successful') {
                    var item = {
                        id: data.result.id,
                        name: node.categoryMap.title,
                        category_id: data.result.category_id,
                        target_id: data.result.target_id,
                        created_at: data.result.created_at,
                        updated_at: data.result.updated_at
                    };
                    node.mapping.push(item);
                    mappingProduct(data.result, "mapping");
                }
                node.isLoading = false;
            });
        } else {
            alert("Please choose a category");
        }
    }

    $scope.findMainTree = function() {
        var url = urlApi + "/service/category/get-main-tree";
        $http.get(url).success(function (response) {
            if (response.status == 'successful') {
                $scope.treeTarget = response.items;
            }
        })
    }

    $scope.removeMapping = function (node, item) {
        var result = confirm("Want to delete?");
        if (result) {
            var url = urlApi + "/api/pets_category_mapping/" + item.id + "?" + tokenField + "=" + serviceToken;
            $http.delete(url).success(function (response) {
                if (response.status == 'successful') {
                    var idx = null;
                    for (var i = 0; i < node.mapping.length; i++) {
                        if (node.mapping[i].category_id == item.category_id) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx != null) {
                        node.mapping.splice(idx, 1);
                    }
                    mappingProduct(item, "delete");
                }
            });
        }
    }

    function mappingProduct(mapping, type) {
        var url = urlApi + "/service/category/mapping-product";
        var data = {
            "site": $scope.filter.targetSite.domain,
            "category_id": mapping.category_id,
            "target_id": mapping.target_id,
            "type": type
        };
        $http.post(url, data).success(function (data) {

        });
    }

    $scope.findTargetTree = function () {
        if ($scope.filter.targetSite) {
            $scope.treedata = [];
            var url = urlApi + '/service/category/get-tree-target?site=' + $scope.filter.targetSite.domain;
            $http.get(url).success(function (response) {
                if (response.status == 'successful') {
                    $scope.treedata = buildTreeData(response.items);
                }
            });
        }

    }

    function buildTreeData(items) {
        for (var i = 0; i < items.length; i++) {
            var children = items[i].children;
            items[i].children = [];
            if (children.length > 0) {
                for (var j = 0; j < children.length; j++) {
                    var ket = buildTreeData(children[j]);
                    items[i].children.push(ket);
                }
            }
        };
        return items;
    }

    $scope.fillTargetTree = function(node) {
        node.treeTarget = $scope.treeTarget;
    }

    this.initialize();
}
