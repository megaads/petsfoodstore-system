system.controller("ProductMatchingController", ProductMatchingController);
/**
 *
 * @param {type} $scope
 * @param {type} $http
 * @param {type} $rootScope
 * @returns {undefined}
 */
function ProductMatchingController($scope, $http, $rootScope, $timeout, Upload) {
    $scope.controllerName = "ProductMatchingController";
    this.__proto__ = new BaseController($scope, $http, $rootScope);
    $scope.items = [];
    $scope.listMatchedProduct = [];
    this.initialize = function(){
        $scope.resetFilter();
        $scope.findMatched();
    };
    $scope.recordsCount = 0;
    $scope.recordsCountSuggestion
    $scope.filterSuggestion = {
        pageId: 0
    }


    $scope.findMatched = function() {
        $('#searchDealButton').button('loading');
        url = api_domain + "/service/product/list-matched";
        $http({
            url: url,
            method: "GET",
            params: $scope.filter,
            header: {
                'Content-Type': 'application/json',
            },
        }).then(
            function(response){
                var items = response.data.data;
                var maxMatchedProduct = 0;
                for (var key in items){
                    var item = items[key];
                    var meta_data = JSON.parse(item.meta_data);
                    for (var k in meta_data.matched_products) {
                        if (k + 1 > maxMatchedProduct) {
                            maxMatchedProduct = parseInt(k) + 1;
                        }
                    }
                    items[key].meta_data = meta_data;
                }
                $scope.listMatchedProduct = [];
                for (var i = 0; i < maxMatchedProduct; i++) {
                    $scope.listMatchedProduct.push(i);
                }
                $scope.items = items;
                $scope.recordsCount = response.data.recordsCount;
                $('#searchDealButton').button('reset');
            }
        );
    };

    $scope.clickTabSuggestion = function () {
        $scope.filterSuggestion.pageId = 0;
        $scope.findSuggestion();
    }

    $scope.findSuggestion = function() {
        url = api_domain + "/service/product/list-suggestion";
        $scope.filterSuggestion.create_from = $scope.filter.create_from;
        $scope.filterSuggestion.create_to = $scope.filter.create_to;
        $http({
            url: url,
            method: "GET",
            params: $scope.filterSuggestion,
            header: {
                'Content-Type': 'application/json',
            },
        }).then(
            function(response){
                var itemsSuggestion = response.data.data;
                var maxSuggestionProduct = 0;
                for (var key in itemsSuggestion){
                    var item = itemsSuggestion[key];
                    var meta_data = JSON.parse(item.meta_data);
                    for (var k in meta_data.similar_products) {
                        if (k + 1 > maxSuggestionProduct) {
                            maxSuggestionProduct = parseInt(k) + 1;
                        }
                    }
                    itemsSuggestion[key].meta_data = meta_data;
                }
                $scope.listSuggestionProduct = [];
                for (var i = 0; i < maxSuggestionProduct; i++) {
                    $scope.listSuggestionProduct.push(i);
                }
                $scope.itemsSuggestion = itemsSuggestion;
                $scope.recordsCountSuggestion = response.data.recordsCount;
            }
        );
    }

    $scope.find = function() {
        $scope.findMatched();
        $scope.findSuggestion();
    };

    $scope.resetFilter = function(){
        var time = Date.now();
        var createFrom = $scope.buildTime(time - 7 * 86400 * 1000, false);
        var createTo = $scope.buildTime(time, false);
        $scope.filter = {
            pageId: 0,
            create_from: createFrom,
            create_to: createTo
        };
        $scope.filterSuggestion = {
            pageId: 0
        }
    }



    $scope.buildParam = function(){
        $scope.params = angular.copy($scope.filter);
        for(var key in $scope.params){
            if($scope.params[key] === ''){
                delete ($scope.params[key]);
            }
        }
    }

    $scope.reset = function() {
        $scope.resetFilter();
        $scope.find();
    }

    $scope.slugify = function(name){
        return name.toString().toLowerCase().trim()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/&/g, '-and-')         // Replace & with 'and'
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-');        // Replace multiple - with single -
    }

    $scope.unMatchProduct = function (item, matchedProductId) {

        var url = api_domain + "/service/product/remove-matched-product";
        $http({
            url: url,
            method: "POST",
            params: {id: item.id, matchedProductId: matchedProductId},
            header: {
                'Content-Type': 'application/json',
            },
        }).then(
            function(response){
                if (response.data.status == 'successful') {
                    showMessage('Success', 'Remove matched successful!', 'success');
                    var matched_products = item.meta_data.matched_products;
                    if (matchedProductId) {
                        for (var k in matched_products) {
                            if (matched_products[k].id == matchedProductId) {
                                matched_products[k] = {};
                                break;
                            }
                        }
                        item.meta_data.matched_products = matched_products;
                    } else {
                        item.meta_data.matched_products = [];
                    }

                } else {
                    showMessage('Fail', 'Remove matched fail!', 'error');
                }
            }
        );
    };
    
    $scope.floor = function (a, b) {
        return Math.floor(a/b);
    };

    $scope.matchSuggestion = function (index, item, suggestionProductId) {
        var url = api_domain + "/service/product/match-suggestion-product";
        $http({
            url: url,
            method: "POST",
            params: {id: item.id, suggestionProductId: suggestionProductId},
            header: {
                'Content-Type': 'application/json',
            },
        }).then(
            function(response){
                if (response.data.status == 'successful') {
                    showMessage('Success', 'Matched successful!', 'success');
                    var similar_products = item.meta_data.similar_products;
                    if (suggestionProductId) {
                        for (var k in similar_products) {
                            if (similar_products[k].id == suggestionProductId) {
                                similar_products[k] = {};
                                break;
                            }
                        }
                        item.meta_data.similar_products = similar_products;
                    } else {
                        $scope.itemsSuggestion.splice(index, 1);
                    }
                } else {
                    showMessage('Fail', 'Match suggestion fail!', 'error');
                }
            }
        );
    };

    this.initialize();
}
